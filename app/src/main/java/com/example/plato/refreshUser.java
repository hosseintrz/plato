package com.example.plato;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicReference;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class refreshUser {
    WeakReference<Activity> activityReference;

    public refreshUser(Activity activity) {
        activityReference = new WeakReference<>(activity);
    }

    Socket socket;
    ObjectInputStream ois;
    DataOutputStream dos;

    public User refresh(String username) {

        AtomicReference<User> user = new AtomicReference<>();
            Thread socketinit=new Thread(() -> {
                try {
                    socket = new Socket(activityReference.get().getString(R.string.ip), 8080);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            socketinit.start();
        try {
            socketinit.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Thread sendUser = new Thread(() -> {
            try {
                synchronized (socket) {
                    dos = new DataOutputStream(socket.getOutputStream());
                    dos.writeUTF("refresh,"+username);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        sendUser.start();
        try {
            sendUser.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Thread getUser = new Thread(() -> {
            Log.d(TAG, "refresh:getuser thread started");
            try {
                synchronized (socket) {

                    Log.d(TAG, "refresh: socket connected");
                    ois = new ObjectInputStream(socket.getInputStream());
                    User newUser = (User) ois.readObject();
                    Log.d(TAG, "refresh: newuser username: " + newUser.getUsername());
                    user.set(newUser);
                }
                Log.d(TAG, "refresh: atomic user username: " + user.get().getUsername());
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        getUser.start();
        try {
            getUser.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "refresh: returning user: " + user.get().getUsername());
        return user.get();
    }


}
