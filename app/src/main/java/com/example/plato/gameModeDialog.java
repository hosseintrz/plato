package com.example.plato;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class gameModeDialog extends DialogFragment {
    Game game;
    Button casual;
    Button ranked;
    public gameModeDialog(Game game) {
        this.game=game;
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_game_mode_dialog, container, false);
        casual=view.findViewById(R.id.casualGame);
        ranked=view.findViewById(R.id.rankedGame);
        casual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent casualGame=new Intent(getActivity(), GameRoomsActivity.class);
            }
        });
        ranked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return view;
    }
}
