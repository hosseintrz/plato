package com.example.plato;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button signUpBtn;
    Button signInBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //comment
        getRef();
        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signUpIntent=new Intent(MainActivity.this,signupActivity.class);
                startActivity(signUpIntent);
            }
        });
        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent=new Intent(MainActivity.this,LoginActivity.class);
                startActivity(signInIntent);
            }
        });
    }

    private void getRef() {
        signUpBtn=findViewById(R.id.mainSignupBtn);
        signInBtn=findViewById(R.id.mainSigninBtn);
    }
}
