package com.example.plato;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import android.util.Base64;

import de.hdodenhof.circleimageview.CircleImageView;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class friendsRecyclerAdapter extends RecyclerView.Adapter<friendsRecyclerAdapter.ViewHolder> {
    private User user;
    private friendClickListener clickListener;
    private static ArrayList<User> friends;
    private Context context;
    public friendsRecyclerAdapter(Context context, ArrayList<User> friends,User user){
        friendsRecyclerAdapter.friends=friends;
        Log.d(TAG, "friendsRecyclerAdapter: friends size"+friends.size());
        this.context=context;
        this.user=user;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        CircleImageView propic;
        TextView username;
        ConstraintLayout contactview;
        private ViewHolder(@NonNull View itemView,friendClickListener clickListener1) {
            super(itemView);
            clickListener=clickListener1;
            propic=itemView.findViewById(R.id.propic);
            username=itemView.findViewById(R.id.friendUsername);
            contactview=itemView.findViewById(R.id.contact);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            clickListener.onClick(getAdapterPosition());
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friendview, parent, false);
        return new ViewHolder(view, clickListener);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder,final int position) {
        byte[] b=Base64.decode((friends.get(position)).getEncodedProfile(),Base64.DEFAULT);
        //byte[] b= Base64.getDecoder().decode(friends.get(position).getEncodedProfile());
        Bitmap bm= BitmapFactory.decodeByteArray(b,0,b.length);
        holder.propic.setImageBitmap(bm);
        holder.username.setText(friends.get(position).getUsername());
    }
    public void updateList(ArrayList<User> newFriend) {
        friends= newFriend;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return user.getFriends().size();
    }


    public interface friendClickListener{
        void onClick(int position);
    }
    public interface friendHoldListener{
        void onHold(int position);
    }
}
