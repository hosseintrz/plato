package com.example.plato;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class User implements Serializable {
    private static final long serialVersionUID = 3345534423L;
    private String username;
    private String password;
    private String email;
    private String encodedProfile;
    private ArrayList<User> friends;
    private ArrayList<User> friendRequests;


    public User(String username, String password, String email) {
        setUsername(username);
        setEmail(email);
        setPassword(password);
        friends = new ArrayList<>();
        friendRequests = new ArrayList<>();
    }

   /* public User(String username, String password, String email, String encodedProfile) {
        setUsername(username);
        setEmail(email);
        setPassword(password);
        setEncodedProfile(encodedProfile);
    }*/

    public String getEncodedProfile() {
        return encodedProfile;
    }

    public void setEncodedProfile(String encodedProfile) {
        this.encodedProfile = encodedProfile;
    }

    public void addFriend(User user) {
        if (!friends.contains(user))
            this.friends.add(user);
    }
    public void removeFriend(User user){
        this.friends.remove(user);
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<User> getFriendRequests() {
        return friendRequests;
    }

    public void addFriendRequests(User userReq) {
        this.friendRequests.add(userReq) ;
    }
    public void removeFriendRequest(User userReq){
        this.friendRequests.remove(userReq);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass())
            return false;
        return this.getUsername().equals(((User) obj).getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(username);
    }

    public ArrayList<User> getFriends() {
        return friends;
    }


}

