package com.example.plato;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class StartGameDialog extends DialogFragment {
    private static final String TAG = "StartGameDialog";
    GameRoom gameroom;
    ArrayList<ImageView> images;
    ArrayList<TextView> names;
    TextView Counter;
    TextView roomName;
    WeakReference<Activity> activityReference;
    int t;

    public StartGameDialog(GameRoom gr) {
        gameroom=gr;
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activityReference=new WeakReference<>(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        t=9;
        View view= inflater.inflate(R.layout.fragment_start_game_dialog, container, false);
        images=new ArrayList<>();
        names=new ArrayList<>();
        images.add(view.findViewById(R.id.player1));
        images.add(view.findViewById(R.id.player2));
        images.add(view.findViewById(R.id.player3));
        images.add(view.findViewById(R.id.player4));
        images.add(view.findViewById(R.id.player5));
        names.add(view.findViewById(R.id.player1Name));
        names.add(view.findViewById(R.id.player2Name));
        names.add(view.findViewById(R.id.player3Name));
        names.add(view.findViewById(R.id.player4Name));
        names.add(view.findViewById(R.id.player5Name));
        roomName=view.findViewById(R.id.dialogroomname);
        Counter=view.findViewById(R.id.counter);
        roomName.setText(gameroom.getName());
        Counter.setText("10");

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //super.onViewCreated(view, savedInstanceState);


    }

    private void updateRooms() {
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
               activityReference.get().runOnUiThread(() -> {

                        RoomUpdate roomupdate = new RoomUpdate(activityReference.get());
                            t--;
                            Counter.setText(t);

                       // ((casualRecyclerAdapter) recyclerview.getAdapter()).updateData(rooms);


                });
                if (t>0)
                    h.postDelayed(this, 1000);
                else
                    Log.d(TAG, "run: finished");
            }
        }, 1000);
    }

}
