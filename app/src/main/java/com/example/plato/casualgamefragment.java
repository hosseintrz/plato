package com.example.plato;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class casualgamefragment extends Fragment implements casualRecyclerAdapter.gameRoomListener {
    private static final String TAG = "casualgamefragment";

    RecyclerView recyclerview;
    public User user;
    ArrayList<GameRoom> rooms;
    Button newRoom;
    Game game;
    boolean updateRooms;
    WeakReference<Activity> activityReference;

    public casualgamefragment() {
        //activityReference=new WeakReference<>(activity);

        /*refreshUser refreshuser = new refreshUser(activity);
        user = refreshuser.refresh();*/
        /*RoomUpdate roomupdate=new RoomUpdate(activity);
        rooms=(ArrayList<GameRoom>) roomupdate.update("getRooms",1);
        Log.d(TAG, "casualgamefragment: rooms size======================="+rooms.size());*/
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activityReference = new WeakReference<>(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_casualgamefragment, container, false);
        //getuser
        String username = (String) getArguments().get("user");
        refreshUser refreshuser = new refreshUser(activityReference.get());
        user = refreshuser.refresh(username);
        //get rooms
        RoomUpdate roomupdate = new RoomUpdate(activityReference.get());
        rooms = (ArrayList<GameRoom>) roomupdate.update("getRooms", 1, username);
        Log.d(TAG, "casualgamefragment: rooms size=======================" + rooms.size());

        game = (Game) getArguments().getSerializable("game");
        Log.d(TAG, "onCreateView: game in casual gamefrag:" + game.getName());
        recyclerview = view.findViewById(R.id.casualRecyclerView);
        recyclerview.setLayoutManager(new LinearLayoutManager(activityReference.get()));
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(new casualRecyclerAdapter(activityReference.get(), this, rooms, user.getUsername()));
        newRoom = view.findViewById(R.id.createNewRoom);

        updateRooms = true;
        updateRooms();

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        // super.onViewCreated(view, savedInstanceState);
        newRoom.setOnClickListener(v -> {
            createRoomDialogFrag dialogFrag = new createRoomDialogFrag(game, user.getUsername());
            dialogFrag.show(getChildFragmentManager(), "create room");
            //update rooms
            /*RoomUpdate roomupdate=new RoomUpdate(activityReference.get());
            rooms=(ArrayList<GameRoom>) roomupdate.update("getRooms",1);
            Log.d(TAG, "onViewCreated: rooms num after updating in casualfrag"+rooms.size());
            ///update adapter
            ((casualRecyclerAdapter) recyclerview.getAdapter()).updateData(rooms);*/
        });
    }

    private void updateRooms() {
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                synchronized (rooms) {
                    RoomUpdate roomupdate = new RoomUpdate(activityReference.get());
                    rooms = (ArrayList<GameRoom>) roomupdate.update("getRooms", 1, game.getName());
                    for (GameRoom gr : rooms) {
                        if (gr.getState().equals(State.FULL)) {
                            Log.d(TAG, "run: executing game room :" + gr.getName());
                            executeRoom(gr);
                        }
                    }
                    activityReference.get().runOnUiThread(() -> {
                        ((casualRecyclerAdapter) recyclerview.getAdapter()).updateData(rooms);

                    });
                }
                if (updateRooms)
                    h.postDelayed(this, 3000);
            }
        }, 3000);
    }

    public void executeRoom(GameRoom gr) {
        gr.setState(State.STARTING);
        StartGameDialog dialog = new StartGameDialog(gr);
        dialog.show(getChildFragmentManager(), "counter");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: paaaaaused");
        updateRooms = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateRooms = true;
        updateRooms();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: update rooms turned off");

    }

    @Override
    public void onRoomClick(int position) {
        Log.d(TAG, "onRoomClick: position=" + position);
        GameRoom room = rooms.get(position);
        Log.d(TAG, "onRoomClick: room.name");

        RoomUpdate roomupdate = new RoomUpdate(getActivity());
        Object serverResponse = roomupdate.update(
                new StringBuilder().append("addPlayer,").append(room.getName()).toString(), 1, user.getUsername());
        if (serverResponse instanceof String)
            Toast.makeText(getContext(), (String) serverResponse, Toast.LENGTH_SHORT).show();
        else
            rooms = (ArrayList<GameRoom>) serverResponse;
        /*if (room.getState().equals(State.WAITING) && !room.getPlayers().contains(user)) {

        } else if (room.getState().equals(State.FULL))
            Toast.makeText(getContext(), "room is full", Toast.LENGTH_SHORT);
        else if (room.getState().equals(State.RUNNING))
            Toast.makeText(getContext(), "going to watch match", Toast.LENGTH_SHORT);*/
        ((casualRecyclerAdapter) recyclerview.getAdapter()).updateData(rooms);
    }

    @Override
    public void onRoomHold(View view, int position) {
        //open menu
        PopupMenu popupmenu = new PopupMenu(activityReference.get(), view);
        popupmenu.getMenuInflater().inflate(R.menu.casualpopupmenu, popupmenu.getMenu());
        popupmenu.show();
        popupmenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.editRoom:
                        editRoomDialog editroom = new editRoomDialog(position, game, user.getUsername());
                        editroom.show(getChildFragmentManager(), "editRoom");
                        break;
                    case R.id.deleteRoom:
                        //delete room
                        break;
                }
                return true;
            }
        });
        //popupmenu.dismiss();
    }
}

class casualRecyclerAdapter extends RecyclerView.Adapter<casualRecyclerAdapter.ViewHolder> {
    gameRoomListener listener;
    ArrayList<GameRoom> rooms;
    private static final String TAG = "casualRecyclerAdapter";
    WeakReference<Activity> activityReference;
    String username;


    public casualRecyclerAdapter(Activity activity, gameRoomListener listener, ArrayList<GameRoom> rooms, String username) {
        this.listener = listener;
        activityReference = new WeakReference<>(activity);
        this.rooms = rooms;
        this.username = username;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_recycler_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GameRoom room = rooms.get(position);
        holder.creator = room.getCreator();
        if (room.getState().equals(State.WAITING))
            holder.Btn.setText("JOIN");
        else if (room.getState().equals(State.FULL))
            holder.Btn.setText("FULL");
        else if (room.getState().equals(State.RUNNING)) {
            holder.Btn.setText("WATCH");
            holder.Btn.setBackgroundColor(activityReference.get().getResources().getColor(R.color.Darkgray));
        } else if (room.getState().equals(State.FINISHED)) {
            holder.Btn.setText("Finished");
            //delete holder
        }
        byte[] encodeByte = Base64.decode(rooms.get(position).getCreator().getEncodedProfile(), Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
        holder.creatorImage.setImageBitmap(bitmap);

        holder.playerNum.setText(room.getPlayersNum() + "/" + room.getPlayerLimit() + " players");
        holder.roomName.setText(room.getName());
        refreshUser refreshuser = new refreshUser(activityReference.get());
        User currentUser = refreshuser.refresh(username);
        if (holder.creator.equals(currentUser)) {
            holder.viewLayout.setBackgroundColor(activityReference.get().getResources()
                    .getColor(R.color.highlightedRoom));
        }
    }

    @Override
    public int getItemCount() {
        return rooms.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        RelativeLayout viewLayout;
        CircleImageView creatorImage;
        TextView roomName;
        Button Btn;
        TextView playerNum;
        User creator;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            creatorImage = itemView.findViewById(R.id.casualGamePic);
            viewLayout = itemView.findViewById(R.id.roomLayout);
            roomName = itemView.findViewById(R.id.casualRoomName);
            Btn = itemView.findViewById(R.id.casualGameBtn);
            playerNum = itemView.findViewById(R.id.casualGamePlayerNum);
            Btn.setOnClickListener(this);
            viewLayout.setOnLongClickListener(this);

//            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onRoomClick(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            Log.d(TAG, "onLongClick: hooooooooooooooooooooooooooooooooooooooooooooooooold");
            refreshUser refreshuser = new refreshUser(activityReference.get());
            User user = refreshuser.refresh(username);
            if (rooms.get(getAdapterPosition()).getCreator().equals(user))
                listener.onRoomHold(v, getAdapterPosition());
            return true;
        }
    }

    public interface gameRoomListener {
        public void onRoomClick(int position);

        public void onRoomHold(View view, int position);
    }

    public void updateData(ArrayList<GameRoom> rooms) {
        this.rooms = rooms;
        notifyDataSetChanged();
    }
}
