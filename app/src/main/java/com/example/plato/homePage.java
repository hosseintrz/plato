package com.example.plato;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class homePage extends AppCompatActivity {
    Button userNum;
    User user;
    private static final String TAG = "homePage";
    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        Intent intent = getIntent();
        String username = intent.getStringExtra("user");
        refreshUser refreshuser = new refreshUser(this);
        user = refreshuser.refresh(username);
        //user = (User) intent.getSerializableExtra("user");
        Log.d(TAG, "onCreate: username in homepage: " + user.getUsername());
        bottomNavigationView = findViewById(R.id.bottomNav);
        bottomNavigationView.setOnNavigationItemSelectedListener(bottomNavMethod);
        //FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        //ft.replace(R.id.container, new homefragment());
        //ft.addToBackStack(null);
        //ft.commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener bottomNavMethod = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.game:
                    fragment = new gamefragment();
                    break;
                case R.id.chat:
                    fragment = new chatfragment();
                    break;
                case R.id.people:
                    fragment = new peoplefragment();
                    break;
                case R.id.gift:
                    fragment = new prizefragment();
                    break;
                default:
                    fragment = new homefragment();
            }


            /*Bundle data = new Bundle();
            data.putSerializable("user", user);
            fragment.setArguments(data);*/
            Bundle data = new Bundle();
            data.putString("user", user.getUsername());
            fragment.setArguments(data);
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();

            //getSupportFragmentManager().popBackStack();
            //  ft.addToBackStack(null);
            //ft.commit();
            return true;
        }
    };
}
