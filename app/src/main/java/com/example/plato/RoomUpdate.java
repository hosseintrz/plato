package com.example.plato;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.util.ArrayList;

public class RoomUpdate {
    WeakReference<Activity> activityReference;
    private static final String TAG = "RoomUpdate";
    public RoomUpdate(Activity activity) {
        activityReference = new WeakReference<>(activity);
    }

    Socket socket = null;
    DataOutputStream dos;
    ObjectInputStream ois;
    GameRoom gameRoom;
    ArrayList<GameRoom> rooms;
    Object out;
    /**
     * send message to server with a task to do on
     * gameRooms and return object of gameRoom or ArrayList<GameRoom>
     *
     * @param op :0 for single room update
     *           1 for rooms list update
     **/
    public Object update(String fullmessage, int op,String username) {
        StringBuilder sb=new StringBuilder();
        sb.append(fullmessage).append(",").append(op).append(",").append(username);
        Thread socketInit = new Thread(() -> {
            try {
                socket = new Socket(activityReference.get().getResources().getString(R.string.ip), 8080);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        socketInit.start();
        try {
            socketInit.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Thread sendMessage = new Thread(() -> {
            try {
                dos = new DataOutputStream(socket.getOutputStream());
                dos.writeUTF(sb.toString());
                dos.flush();
                Log.d(TAG, "update: full message sent to roomupdatere is: "+fullmessage);


            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        sendMessage.start();
        try {
            sendMessage.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Thread receiveMessage = new Thread(() -> {
            try {
                ois = new ObjectInputStream(socket.getInputStream());
                Object inp = ois.readObject();
                out=inp;
                /*if (op == 0)
                    gameRoom = (GameRoom) inp;
                else if (op == 1)
                    rooms = (ArrayList<GameRoom>) inp;

*/
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        receiveMessage.start();
        try {
            receiveMessage.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        /*if (op == 0)
            return gameRoom;
        else
            return rooms;*/
        return out;
    }
}
