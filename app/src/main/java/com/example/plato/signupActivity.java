package com.example.plato;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.Socket;

public class signupActivity extends AppCompatActivity {
    private static final String TAG = "signupActivity";
    EditText ETusername;
    EditText ETpassword;
    EditText ETemail;
    Button signupBtn;
    String username;
    String password;
    String email;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);
        getRef();

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = ETusername.getText().toString();
                password = ETpassword.getText().toString();
                email = ETemail.getText().toString();
                SendData sendData = new SendData(signupActivity.this);
                sendData.execute(username, password, email);
            }
        });
        //validate(username,password,email);

    }

    private void getRef() {
        ETusername = findViewById(R.id.signupUsername);
        ETpassword = findViewById(R.id.signupPassword);
        ETemail = findViewById(R.id.signupEmail);
        signupBtn = findViewById(R.id.signupBtn);
    }

}

class SendData extends AsyncTask<String, String, Boolean> {
    private Socket s;
    private DataInputStream dis;
    private DataOutputStream dos;
    private WeakReference<signupActivity> activityReference;
    private User newUser;
    private static final String TAG = "SendData";
    boolean isDone;


    SendData(signupActivity activity) {
        activityReference = new WeakReference<>(activity);
    }

    @Override
    protected Boolean doInBackground(String... input) {
        final String username = input[0];
        final String password = input[1];
        final String email = input[2];
        try {
            s = new Socket(activityReference.get().getResources().getString(R.string.ip), 8080);


            Thread sendMessage = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        StringBuilder message = new StringBuilder();
                        message.append("signUp,")
                                .append(username).append(" ,")
                                .append(password).append(" ,")
                                .append(email).append(" ");
                        synchronized (s) {
                            dos = new DataOutputStream(s.getOutputStream());
                            dos.writeUTF(message.toString());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            sendMessage.start();

            try {
                sendMessage.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            Thread receiveMessage = new Thread(new Runnable() {
                String reply;

                @Override
                public void run() {
                    try {
                        synchronized (s) {
                            dis = new DataInputStream(s.getInputStream());
                            reply = dis.readUTF();
                        }
                        if (!(reply.startsWith("welcome") || reply.startsWith("user exists"))) {
                            String[] errors = reply.split(",");
                            for (String error : errors) {
                                if (error.contains("emailEmpty")) {
                                    setErrorOnUi(activityReference.get().ETemail, "email can't be empty");
                                }
                                if (error.contains("usernameEmpty")) {
                                    setErrorOnUi(activityReference.get().ETusername, "username can't be empty");
                                }
                                if (error.contains("passwordEmpty")) {
                                    setErrorOnUi(activityReference.get().ETpassword, "password can't be empty");
                                }
                                if (error.contains("passwordShort")) {
                                    setErrorOnUi(activityReference.get().ETpassword, "password must be more than 5 characters");
                                }
                            }
                            isDone = false;
                        } else if (reply.startsWith("user exists")) {
                            //new AlertDialog.Builder(new ContextThemeWrapper(activityReference.get(),android.R.style.Theme_dialog));
                            activityReference.get().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    new AlertDialog.Builder(activityReference.get())
                                            .setTitle("Error")
                                            .setMessage("this username exists")
                                            .setCancelable(true)
                                            .show();
                                }
                            });
                            isDone = false;
                        } else if (reply.startsWith("welcome")) {
                            newUser = new User(username, password, email);
                            Intent selectPhoto = new Intent(activityReference.get(), SelectPhoto.class);
                            selectPhoto.putExtra("user", newUser.getUsername());
                            activityReference.get().startActivity(selectPhoto);
                            isDone = true;
                        } else
                            isDone = false;


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
            receiveMessage.start();
            try {
                receiveMessage.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return isDone;
    }

    @Override
    protected void onPostExecute(Boolean done) {
        try {
            if (done)
                Toast.makeText(activityReference.get(), "sign up was successful", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(activityReference.get(), "sign up Error", Toast.LENGTH_SHORT).show();

            dis.close();
            dos.close();
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onPostExecute(done);
    }

    /**
     * put the error string in edittext onUiThread
     *
     * @param edittext the edittext that error is going to publish in
     * @param error    the message of error
     */
    private void setErrorOnUi(final EditText edittext, final String error) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                edittext.setError(error);
            }
        };
        activityReference.get().runOnUiThread(runnable);
    }

}