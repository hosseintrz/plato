package com.example.plato;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class createRoomDialogFrag extends DialogFragment {
    private static final String TAG = "createRoomDialogFrag";

    String roomName;
    EditText roomNameE;

    TextView gameNameT;
    RadioGroup radioGroup;
    Button createBtn;
    int playerNum;
    Game game;
    ArrayList<GameRoom> rooms;
    WeakReference<Activity> activityReference;
    RecyclerView recyclerView;
String username;
    public createRoomDialogFrag(Game game,String username) {
        // Required empty public constructor
        this.username=username;
        this.game = game;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activityReference = new WeakReference<>(getActivity());
        recyclerView = activityReference.get().findViewById(R.id.casualRecyclerView);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_room_dialog, container, false);
        gameNameT = view.findViewById(R.id.dialogGamename);
        roomNameE = view.findViewById(R.id.dialogRoomName);
        radioGroup = view.findViewById(R.id.radiogroup);
        createBtn = view.findViewById(R.id.createBtn);
        gameNameT.setText(game.getName());
        RadioButton rb3 = view.findViewById(R.id.rb3);
        RadioButton rb4 = view.findViewById(R.id.rb4);
        RadioButton rb5 = view.findViewById(R.id.rb5);
        if (!(game instanceof DotBox)) {
            rb3.setEnabled(false);
            rb4.setEnabled(false);
            rb5.setEnabled(false);
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //super.onViewCreated(view, savedInstanceState);
        createBtn.setOnClickListener(v -> {
            roomName = roomNameE.getText().toString();
            switch (radioGroup.getCheckedRadioButtonId()) {
                case R.id.rb2:
                    playerNum = 2;
                    break;
                case R.id.rb3:
                    playerNum = 3;
                    break;
                case R.id.rb4:
                    playerNum = 4;
                    break;
                case R.id.rb5:
                    playerNum = 5;
                    break;
            }
            if (roomName.isEmpty())
                roomNameE.setError("Enter Room Name");
            else {
                RoomUpdate roomupdate = new RoomUpdate(getActivity());
                StringBuilder sb = new StringBuilder();
                sb.append("addRoom,").append(game.getName()).append(",")
                        .append(playerNum).append(",").append(roomName);
                Object Input = roomupdate.update(sb.toString(), 1,username);
                if (Input instanceof String) {
                    Toast.makeText(activityReference.get(), "you have active rooms", Toast.LENGTH_SHORT).show();
                } else {
                    rooms = (ArrayList<GameRoom>) Input;
                    ((casualRecyclerAdapter) recyclerView.getAdapter()).updateData(rooms);
                }
            }
            getDialog().dismiss();
        });
    }
}
