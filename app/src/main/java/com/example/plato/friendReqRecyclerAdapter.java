package com.example.plato;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import android.util.Base64;

import de.hdodenhof.circleimageview.CircleImageView;

public class friendReqRecyclerAdapter extends RecyclerView.Adapter<friendReqRecyclerAdapter.ViewHolder> {
    private User user;
    private friendReqListener friendReqListener;
    private Context context;
    private ArrayList<User> friendReqs;

    public friendReqRecyclerAdapter(Context context, User user,friendReqListener listener) {
        this.context = context;
        this.user = user;
        this.friendReqs = user.getFriendRequests();
        friendReqListener=listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friendreqview, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        byte[] b=Base64.decode(friendReqs.get(position).getEncodedProfile(),Base64.DEFAULT);
        //byte[] b1 = Base64.getDecoder().decode(friendReqs.get(position).getEncodedProfile());
        Bitmap bm = BitmapFactory.decodeByteArray(b, 0, b.length);
        holder.propic.setImageBitmap(bm);
        holder.username.setText(friendReqs.get(position).getUsername());
    }

    @Override
    public int getItemCount() {
        return friendReqs.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CircleImageView propic;
        private TextView username;
        private ConstraintLayout contactview;
        private Button accept;
        private Button reject;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            //friendReqListener = clicklistener1;
            propic = itemView.findViewById(R.id.reqpropic);
            username = itemView.findViewById(R.id.friendreqUsername);
            contactview = itemView.findViewById(R.id.reqcontact);
            accept = itemView.findViewById(R.id.accept);
            reject = itemView.findViewById(R.id.reject);
            accept.setOnClickListener(this);
            reject.setOnClickListener(this);
            // itemView.setOnClickListener(this);
        }

        /*public void removed(int pos) {
            notifyItemRemoved(pos);
        }
        public void inserted(int pos){
            notifyItemInserted(pos);
        }
*/
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.accept:
                    friendReqListener.onClick(getAdapterPosition(), true);
                    break;
                case R.id.reject:
                    friendReqListener.onClick(getAdapterPosition(), false);
                    break;
            }
            //notifyDataSetChanged();

        }
    }

    interface friendReqListener {
        void onClick(int position, boolean accept);
    }
    public void updateList(ArrayList<User> newFriendReqs) {
        friendReqs = newFriendReqs;
        notifyDataSetChanged();
    }
}
