package com.example.plato;

import java.io.Serializable;
import java.util.ArrayList;

public class Game implements Serializable {
    String encodedPic;
    ArrayList<User> topScorers;
    String name;

    public String getName() {
        return name;
    }

    public Game() {
        topScorers = new ArrayList<>();
    }

    public String getEndodedPic() {
        return encodedPic;
    }
}

class XO extends Game {
    public XO() {
        this.name="XO";
    }
}

class DotBox extends Game {
    public DotBox(){
        this.name="DotBox";
    }
}

class WordGuess extends Game {
    public WordGuess(){
        this.name="WordGuess";
    }
}

class BattleShip extends Game {
    public BattleShip(){
        this.name="battleship";
    }
}
