package com.example.plato;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;


/**
 * A simple {@link Fragment} subclass.
 */
public class homefragment extends Fragment {
    private static final String TAG = "homefragment";
    User user;

    public homefragment() {
        // Required empty public constructor
    }

    Button signout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Bundle bundle = getArguments();
        String username =bundle.getString("user");
        refreshUser refreshuser = new refreshUser(getActivity());
        user = refreshuser.refresh(username);

        View view = inflater.inflate(R.layout.fragment_homefragment, container, false);
        signout = view.findViewById(R.id.signout);
        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread signout1 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Socket socket = new Socket(getActivity().getString(R.string.ip), 8080);
                            DataOutputStream dis = new DataOutputStream(socket.getOutputStream());
                            dis.writeUTF("signout");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                signout1.start();
                try {
                    signout1.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }
}
