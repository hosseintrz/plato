package com.example.plato;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class GameRoomsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tablayout;
    ViewPager2 viewpager2;
    Activity mactivity;
    String username;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_rooms);
        Intent intent=getIntent();
        username=intent.getStringExtra("user");
        String gamename=intent.getStringExtra("gamename");
        mactivity=this;
        toolbar = findViewById(R.id.gameRoomToolbar);
        toolbar.setTitle(gamename);
        setSupportActionBar(toolbar);
        tablayout = findViewById(R.id.gameTabLayout);
        viewpager2 = findViewById(R.id.gameRoomViewPager);
        viewpager2.setAdapter(new RoomViewPagerAdapter(this,gamename));
        TabLayoutMediator tlm = new TabLayoutMediator(tablayout, viewpager2, (tab, position) -> {
            switch (position) {
                case 1:
                    tab.setText("RANKED");
                    break;
                case 2:
                    tab.setText("LEADERBOARD");
                    break;
                default:
                    tab.setText("CASUAL");
            }
        });
        tlm.attach();
    }

    private class RoomViewPagerAdapter extends FragmentStateAdapter {
        String gamename;
        Game game;
        public RoomViewPagerAdapter(@NonNull FragmentActivity fragmentActivity,String gamename) {
            super(fragmentActivity);
            this.gamename=gamename;
            switch (gamename){
                case"XO":
                    game=new XO();
                    break;
                case"DotBox":
                    game=new DotBox();
                    break;
                case "BattleShip":
                    game=new BattleShip();
                    break;
                case "WordGuess":
                    game=new WordGuess();
                    break;
            }
        }
        @NonNull
        @Override
        public Fragment createFragment(int position) {
            Fragment fragment;
            switch (position) {
                case 1:
                    fragment = new rankedgamefragment(mactivity);
                    break;
                case 2:
                    fragment = new leaderboardfragment(mactivity);
                    break;
                default:
                    fragment = new casualgamefragment();
                    break;
            }
            Bundle data=new Bundle();
            data.putSerializable("game",game);
            data.putString("user",username);
            fragment.setArguments(data);
            return fragment;
        }

        @Override
        public int getItemCount() {
            return 3;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.casualmenu, menu);
        return true;
    }
}
