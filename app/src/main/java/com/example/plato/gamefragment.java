package com.example.plato;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class gamefragment extends Fragment {
    ImageView XoGame;
    ImageView DotBoxGame;
    User user;
    String username;
    public gamefragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_gamefragment, container, false);
        username=(String)getArguments().get("user");
        XoGame=view.findViewById(R.id.XoGame);
        DotBoxGame=view.findViewById(R.id.DotBoxGame);
        XoGame.setOnClickListener(v -> {
            Intent GameRoom=new Intent(getContext(),GameRoomsActivity.class);
            GameRoom.putExtra("gamename","XO");
            GameRoom.putExtra("user",username);
            startActivity(GameRoom);
        });
        DotBoxGame.setOnClickListener(v -> {
            Intent GameRoom=new Intent(getContext(),GameRoomsActivity.class);
            GameRoom.putExtra("gamename","DotBox");
            GameRoom.putExtra("user",username);
            startActivity(GameRoom);
        });
        return view;
    }
}
