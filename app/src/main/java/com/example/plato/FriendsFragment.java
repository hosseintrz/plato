package com.example.plato;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsFragment extends Fragment implements friendsRecyclerAdapter.friendClickListener{


    RecyclerView friendsRecyclerView;
    User user;
    ArrayList<User> friends;
    Context context;
    FragmentActivity mActivity;
    FloatingActionButton addFriendBtn;
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context=context;
        if (context instanceof Activity){
            mActivity =(FragmentActivity) context;
        }
    }

    public FriendsFragment() {
        // Required empty public constructor
    }
/*
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent=getActivity().getIntent();
        user= (User) intent.getSerializableExtra("user");
        friends=user.getFriends();
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_friends, container, false);

        String username=(String)getArguments().getSerializable("user");
        refreshUser refreshuser=new refreshUser(getActivity());
        user=refreshuser.refresh(username);
        Log.d(TAG, "onCreateView: username in friends: "+user.getUsername());
        friends=user.getFriends();

        friendsRecyclerView = view.findViewById(R.id.friendsRecyclerView);
        friendsRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        friendsRecyclerView.setAdapter(new friendsRecyclerAdapter(view.getContext(), friends,user));
        friendsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        addFriendBtn=view.findViewById(R.id.addFriend);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //super.onViewCreated(view, savedInstanceState);
        addFriendBtn.setOnClickListener(v -> {
            addFriendDialog dialog=new addFriendDialog(user);
            dialog.show(getChildFragmentManager(),"Add Friend");
        });

    }

    @Override
    public void onClick(int position) {
        //open chat intent
        Log.d(TAG, "onClick: username="+friends.get(position).getUsername());
    }



    /*public void update(){
        refreshUser refreshuser=new refreshUser(getActivity());
        user=refreshuser.refresh();
        ((friendsRecyclerAdapter)friendsRecyclerView.getAdapter()).updateList(user.getFriends());
    }*/
}
