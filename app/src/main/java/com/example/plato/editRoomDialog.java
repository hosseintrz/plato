package com.example.plato;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class editRoomDialog extends DialogFragment {

    String roomName;
    EditText roomNameE;

    TextView gameNameT;
    RadioGroup radioGroup;
    Button editBtn;
    int playerNum;
    Game game;
    ArrayList<GameRoom> rooms;
    WeakReference<Activity> activityReference;
    RecyclerView recyclerView;
    GameRoom room;
    int position;
    String username;
    public editRoomDialog(int position,Game game,String username) {
        // Required empty public constructor
        this.username=username;
        this.position=position;
        this.game=game;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activityReference = new WeakReference<>(getActivity());
        recyclerView = activityReference.get().findViewById(R.id.casualRecyclerView);
        RoomUpdate roomupdate=new RoomUpdate(activityReference.get());
        rooms= (ArrayList<GameRoom>) roomupdate.update("getRooms",0,username);
        room=rooms.get(position);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_edit_room_dialog, container, false);
        gameNameT= view.findViewById(R.id.editDialogGamename);
        radioGroup =view.findViewById(R.id.editRadiogroup);
        roomNameE=view.findViewById(R.id.editDialogRoomName);
        editBtn=view.findViewById(R.id.editRoomBtn);
        // Inflate the layout for this fragment
        int id;
        switch(room.getPlayersNum()){
            case 3:id=R.id.erb3;break;
            case 4:id=R.id.erb4;break;
            case 5:id=R.id.erb5;break;
            default:id=R.id.erb2;break;
        }
        RadioButton erb2=view.findViewById(R.id.erb2);
        RadioButton erb3=view.findViewById(R.id.erb3);
        RadioButton erb4=view.findViewById(R.id.erb4);
        RadioButton erb5=view.findViewById(R.id.erb5);
        if (!(game instanceof DotBox)) {
            erb3.setEnabled(false);
            erb4.setEnabled(false);
            erb5.setEnabled(false);
        }
        RadioButton checked=view.findViewById(id);
        checked.setChecked(true);
        roomNameE.setText(room.getName());
        gameNameT.setText(room.getGame().getName());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        /*super.onViewCreated(view, savedInstanceState);*/
        editBtn.setOnClickListener(v -> {
            roomName=roomNameE.getText().toString();
            switch (radioGroup.getCheckedRadioButtonId()) {
                case R.id.erb2:
                    playerNum = 2;
                    break;
                case R.id.erb3:
                    playerNum = 3;
                    break;
                case R.id.erb4:
                    playerNum = 4;
                    break;
                case R.id.erb5:
                    playerNum = 5;
                    break;
            }
            if (roomName.isEmpty())
                roomNameE.setError("Enter Room Name");
            else if(roomName.length()>12)
                roomNameE.setError("too long");
            else {
                RoomUpdate roomUpdate = new RoomUpdate(activityReference.get());
                StringBuilder sb = new StringBuilder();
                sb.append("editRoom,").append(room.getName()).append(",")
                        .append(roomName).append(",").append(playerNum).append(",").append(room.getGame().getName());
                rooms = (ArrayList<GameRoom>) roomUpdate.update(sb.toString(), 1,username);
                ((casualRecyclerAdapter) recyclerView.getAdapter()).updateData(rooms);
            getDialog().dismiss();
            }
        });
    }
}
