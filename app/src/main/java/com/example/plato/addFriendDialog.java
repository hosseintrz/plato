package com.example.plato;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicReference;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class addFriendDialog extends DialogFragment {
    User user;
    TextView usernameT;
    Button addFriendBtn;
    private static final String TAG = "addFriendDialog";

    public addFriendDialog(User user) {
        // Required empty public constructor
        this.user = user;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_friend_dialog, container, false);
        // user=(User)getArguments().getSerializable("user");

        usernameT = view.findViewById(R.id.usernameSearch);
        addFriendBtn = view.findViewById(R.id.addFriendBtn);
        addFriendBtn.setOnClickListener(v -> {
            String username = usernameT.getText().toString();
            if (username.isEmpty()) {
                usernameT.setError("enter username");
            } else {
                addFriendReq addFriendreq = new addFriendReq(getActivity());
                addFriendreq.execute(username);
            }
        });
        return view;
    }

    class addFriendReq extends AsyncTask<String, String, String> {
        String finalReply;
        Socket socket;
        DataInputStream dis;
        DataOutputStream dos;
        String username1;
        WeakReference<Activity> activityReference;

        public addFriendReq(Activity activity) {
            activityReference = new WeakReference<Activity>(activity);
        }

        @Override
        protected String doInBackground(String... strings) {
            username1 = strings[0];


            try {
                socket = new Socket(getActivity().getResources().getString(R.string.ip), 8080);
            } catch (
                    IOException e) {
                e.printStackTrace();
            }
            Thread sendReq = new Thread(() -> {
                StringBuilder message = new StringBuilder();
                message.append("addFriend,").append(username1).append(",").append(user.getUsername());
                Log.d(TAG, "sendRequest: message sent is : " + message.toString());
                synchronized (socket) {
                    try {
                        dos = new DataOutputStream(socket.getOutputStream());
                        dos.writeUTF(message.toString());
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            });
            sendReq.start();
            try {
                sendReq.join();
            } catch (
                    InterruptedException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "sendRequest: sending request thread finished");
            Thread receiveReply = new Thread(() -> {
                Log.d(TAG, "sendRequest: Thread receive started");

                String reply = null;
                synchronized (socket) {
                    try {
                        dis = new DataInputStream(socket.getInputStream());
                        reply = dis.readUTF();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                finalReply=reply;
                Log.d(TAG, "sendRequest: server final reply is " + finalReply);

            });
            receiveReply.start();
            try {
                receiveReply.join();
            } catch (
                    InterruptedException e) {
                e.printStackTrace();
            }
            return finalReply;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            switch (s) {
                case "request sent":
                    Toast.makeText(getContext(), "request sent to " + username1, Toast.LENGTH_SHORT).show();
                    break;
                case "userNotFound":
                    Toast.makeText(getContext(), username1 + " doesnt exists", Toast.LENGTH_SHORT).show();
                    break;
                case "alreadyExists":
                    Toast.makeText(getContext(), username1 + " is already your friend", Toast.LENGTH_SHORT).show();
                    break;
                case "its your username":
                    Toast.makeText(getContext(),  " its your own username", Toast.LENGTH_SHORT).show();
                    break;
            }
            getDialog().dismiss();
            try {
                socket.close();
                dis.close();
                dos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
