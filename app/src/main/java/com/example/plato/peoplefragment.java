package com.example.plato;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;


import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.hdodenhof.circleimageview.CircleImageView;

import static androidx.constraintlayout.widget.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class peoplefragment extends Fragment {
    private static final String TAG = "peoplefragment";

    public peoplefragment() {
        // Required empty public constructor
    }

    private MaterialToolbar toolbar;
    private ViewPager2 viewPager2;
    private TabLayout tablayout;
    static User user;
    CircleImageView propic;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String username=(String)getArguments().get("user");
        refreshUser refreshuser = new refreshUser(getActivity());
        user = refreshuser.refresh(username);
        //user=(User)getArguments().getSerializable("user");
        Log.d(TAG, "onCreateView: username in peoplefragment= " + user.getUsername());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_peoplefragment, container, false);
        propic = view.findViewById(R.id.propic);
        byte[] b = Base64.decode(user.getEncodedProfile(), Base64.DEFAULT);
        Bitmap bm = BitmapFactory.decodeByteArray(b, 0, b.length);
        Bitmap bm2 = ScaleDown.scaleDownBitmap(bm, 100, getContext());
        propic.setImageBitmap(bm2);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar = getView().findViewById(R.id.topAppBar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        viewPager2 = getActivity().findViewById(R.id.friendsViewPager2);
        viewPager2.setAdapter(new viewPagerAdapter(getActivity()));
        tablayout = getActivity().findViewById(R.id.friendstablayout);
        TabLayoutMediator tm = new TabLayoutMediator(tablayout, viewPager2, (tab, position) -> {
            switch (position) {
                case 1:
                    tab.setText(R.string.request);
                    BadgeDrawable bd = tab.getOrCreateBadge();
                    bd.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.red));
                    bd.setNumber(user.getFriendRequests().size());
                    bd.setMaxCharacterCount(3);
                    if (bd.getNumber() > 0)
                        bd.setVisible(true);
                    else
                        bd.setVisible(false);
                    break;
                default:
                    tab.setText(R.string.friends);
            }
        });
        tm.attach();
        /*viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback());*/
        /*viewPager2.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {

            }

            @Override
            public void onViewDetachedFromWindow(View v) {

            }
        });*/
    }
}

class viewPagerAdapter extends FragmentStateAdapter {

    public viewPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fm = null;
        switch (position) {
            case 1:
                fm = new FriendReqFragment();
                break;
            default:
                fm = new FriendsFragment();
        }
        Bundle data = new Bundle();
        data.putString("user", peoplefragment.user.getUsername());
        fm.setArguments(data);
        Log.d(TAG, "createFragment: a fragment created");
        return fm;
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}

class ScaleDown {
    public static Bitmap scaleDownBitmap(Bitmap photo, int newHeight, Context context) {

        final float densityMultiplier = context.getResources().getDisplayMetrics().density;

        int h = (int) (newHeight * densityMultiplier);
        int w = (int) (h * photo.getWidth() / ((double) photo.getHeight()));

        photo = Bitmap.createScaledBitmap(photo, w, h, true);

        return photo;
    }
}