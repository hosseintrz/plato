package com.example.plato;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.net.Socket;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class SelectPhoto extends AppCompatActivity {
    private static final String TAG = "SelectPhoto";
    static ImageView profile;
    Button gallery;
    Button finish;
    ImageView pro1,pro2,pro3,pro4,pro5,pro6;
    private static final int PICK_IMAGE = 100;
    Uri imageuri;
    static User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_photo);
        //this method is about get items id
        getRef();
        //get input from Signup activity
        Intent intent=getIntent();
        String username=intent.getStringExtra("user");
        Log.d(TAG, "onCreate: username in selectphoto is ");
        refreshUser refreshuser=new refreshUser(this);
        user=refreshuser.refresh(username);
        Log.d(TAG, "onCreate: currnet username: "+user.getUsername());
        //user=(User)intent.getSerializableExtra("user");
        //
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        opengallery();
                    }
                }).start();
            }
        });
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setProfile setProfile = new setProfile(SelectPhoto.this);
                setProfile.execute(user.getEmail(),user.getUsername(),user.getPassword());
            }
        });
        //click on each photo for select profile
        pro1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.setImageResource(R.drawable.pro1);
            }
        });
        pro2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.setImageResource(R.drawable.pro2);
            }
        });
        pro3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.setImageResource(R.drawable.pro3);
            }
        });
        pro4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.setImageResource(R.drawable.pro4);
            }
        });
        pro5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.setImageResource(R.drawable.pro5);
            }
        });
        pro6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profile.setImageResource(R.drawable.pro6);
            }
        });

    }

    //open gallery for select photo
    private void opengallery(){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery , PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode , int resultCode , Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageuri = data.getData();
            profile.setImageURI(imageuri);
        }
    }

    private void getRef() {
        gallery = (Button) findViewById(R.id.galleryBtn);
        finish = (Button) findViewById(R.id.finishBtn);
        profile = (ImageView) findViewById(R.id.profile);
        pro1 = (ImageView) findViewById(R.id.pro1);
        pro2 = (ImageView) findViewById(R.id.pro2);
        pro3 = (ImageView) findViewById(R.id.pro3);
        pro4 = (ImageView) findViewById(R.id.pro4);
        pro5 = (ImageView) findViewById(R.id.pro5);
        pro6 = (ImageView) findViewById(R.id.pro6);
    }

}


class setProfile extends AsyncTask<String,String,Boolean>  {
    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;
    private User user;
    private String servermessage;
    private String encodedProfile;
    private WeakReference<SelectPhoto> activityReference;
    boolean isDone;

    setProfile(SelectPhoto activity) {
        activityReference = new WeakReference<SelectPhoto>(activity);
    }

    @Override
    protected Boolean doInBackground(String... strings) {
        try {
            socket = new Socket(activityReference.get().getResources().getString(R.string.ip), 8080);

            user = new User(strings[1],strings[2],strings[0]);

            Thread sendprofile = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        //encodeing selected profile
                        SelectPhoto.profile.buildDrawingCache();
                        Bitmap bm = SelectPhoto.profile.getDrawingCache();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
                        byte[] b = baos.toByteArray();
                        encodedProfile = Base64.encodeToString(b, Base64.DEFAULT);
                        StringBuilder finalmessage = new StringBuilder();
                        //final message for send to Server
                        finalmessage.append("selectprofile,").append(user.getUsername()).append(",").append(encodedProfile).append(",");
                        Log.d(TAG, "run: finamessage size"+(finalmessage.toString()).length());

                        synchronized (socket) {
                            dos = new DataOutputStream(socket.getOutputStream());
                            //dos.writeUTF(finalmessage.toString());
                            byte[] message=(finalmessage.toString()).getBytes();
                            dos.writeUTF("selectprofile,");
                            dos.flush();
                            dos.writeInt(message.length);
                            dos.flush();
                            dos.write(message);
                            dos.flush();
                            Log.d(TAG, "run: got here");
                            //dos.close();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                        //Toast.makeText(activityReference.get(), "Server is offline", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            sendprofile.start();

            try {
                sendprofile.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Thread serverMessage = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {

                        synchronized (socket){
                            dis = new DataInputStream(socket.getInputStream());
                            servermessage = dis.readUTF();
//                            dis.close();
                        }

                        if (servermessage.equals("received")) {
                            //set profile in User class
                            user.setEncodedProfile(encodedProfile);

                            isDone = true;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            serverMessage.start();

            try {
                serverMessage.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return isDone;
    }

    @Override
    protected void onPostExecute(Boolean done) {
        try {
            if (done) {
                //just to inform the user
                Toast.makeText(activityReference.get(), "Uploaded", Toast.LENGTH_SHORT).show();
                //move to homepage
                Intent intent1 = new Intent(activityReference.get(), homePage.class);
                intent1.putExtra("user", user.getUsername());
                activityReference.get().startActivity(intent1);
            } else {
                //just to inform the user
                Toast.makeText(activityReference.get(), "Not Uploaded", Toast.LENGTH_SHORT).show();
            }
            socket.close();
            dos.close();
            dis.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        super.onPostExecute(done);
    }
}
