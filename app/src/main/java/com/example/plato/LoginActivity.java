package com.example.plato;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.Socket;

public class LoginActivity extends AppCompatActivity {

    EditText ETusername;
    EditText ETpassword;
    Button loginBtn;
    String username;
    String password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getRef();
        loginBtn.setOnClickListener(v -> {
            username = ETusername.getText().toString();
            password = ETpassword.getText().toString();
            sendData senddata = new sendData(LoginActivity.this);
            senddata.execute(username, password);
        });
    }

    private void getRef() {
        ETusername = findViewById(R.id.loginUsername);
        ETpassword = findViewById(R.id.loginPassword);
        loginBtn = findViewById(R.id.loginBtn);
    }


    class sendData extends AsyncTask<String, String, Boolean> {
        private static final String TAG = "loginSendData";
        Socket s;
        DataOutputStream dos;
        DataInputStream dis;
        ObjectInputStream ois;
        WeakReference<LoginActivity> activityReference;
        boolean isDone;
        User user;

        public sendData(LoginActivity activity) {
            activityReference = new WeakReference<>(activity);
        }

        @Override
        protected Boolean doInBackground(String... input) {
            try {
                s = new Socket(activityReference.get().getResources().getString(R.string.ip), 8080);
                final String username = input[0];
                final String password = input[1];

                Thread sendMessage = new Thread(() -> {
                    try {
                        StringBuilder message = new StringBuilder();
                        message.append("login,")
                                .append(username).append(" ,")
                                .append(password).append(" ");
                        synchronized (s) {
                            dos = new DataOutputStream(s.getOutputStream());
                            dos.writeUTF(message.toString());
                            Log.d(TAG, "doInBackground: message sent: " + message.toString());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                });
                sendMessage.start();
                try {
                    sendMessage.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Thread getUser = new Thread(() -> {
                    try {
                        Log.d(TAG, "doInBackground: reading object !!");
                        synchronized (s) {
                            ois = new ObjectInputStream(s.getInputStream());
                            //Log.d(TAG, "doInBackground:received object: "+ois.readObject().toString());
                            user = (User) ois.readObject();
                            ois.close();
                        }
                        isDone = true;
                        Log.d(TAG, "doInBackground: username::" + user.getUsername());
                    } catch (ClassNotFoundException | IOException e) {
                        e.printStackTrace();
                    }
                });

                Thread receiveMessage = new Thread(() -> {
                    try {
                        String reply;
                        synchronized (s) {
                            dis = new DataInputStream(s.getInputStream());
                            Log.d(TAG, "doInBackground: getting reply from server");
                            reply = dis.readUTF();
                            Log.d(TAG, "doInBackground: reply is " + reply);
                        }
                        //Log.d(TAG, "out of synchronized block: reply is " + reply);
                        switch (reply) {
                            case "loginOk":
                                getUser.start();
                                getUser.join();
                                break;
                            case "wrongPassword":
                                runOnUiThread(() -> activityReference.get().ETpassword.setError("wrong Password"));
                                break;
                            case "userNotFound":
                                runOnUiThread(() -> activityReference.get().ETusername.setError("user not found"));
                                break;
                        }
                    } catch (IOException | InterruptedException e) {
                        e.printStackTrace();
                    }

                });
                receiveMessage.start();

                try {
                    Log.d(TAG, "doInBackground: is going to wait");
                    receiveMessage.join();
                    Log.d(TAG, "doInBackground: resumed from receiveMessage");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
            return isDone;
        }

        @Override
        protected void onPostExecute(Boolean done) {
            //super.onPostExecute(done);
            try {
                dis.close();
                dos.close();
                s.close();
                if (done) {
                    Log.d(TAG,"onPostExecute: changing activity");
                  //  Log.d(TAG, "onPostExecute: user friendreqs" +user.getFriendRequests().size());
                    //Log.d(TAG, "onPostExecute:friend reqquuuuuuest "+user.getFriendRequests().get(0).getUsername());
                    Toast.makeText(activityReference.get(), "logged in", Toast.LENGTH_SHORT).show();
                    Intent mainIntent = new Intent(activityReference.get(), homePage.class);
                    mainIntent.putExtra("user", user.getUsername());
                    activityReference.get().startActivity(mainIntent);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
