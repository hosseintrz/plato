package com.example.plato;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class FriendReqFragment extends Fragment implements friendReqRecyclerAdapter.friendReqListener {
    private static final String TAG = "FriendReqFragment";
    User user;
    ArrayList<User> friendReqs;
    RecyclerView reqRecyclerView;
    RequestListener reqlistener;


    public FriendReqFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String username = (String) getArguments().getSerializable("user");
        refreshUser refreshuser=new refreshUser(getActivity());
        user=refreshuser.refresh(username);
        Log.d(TAG, "onCreateView: username in friendsreq: "+user.getUsername());
        View view= inflater.inflate(R.layout.fragment_friend_req, container, false);
        friendReqs=user.getFriendRequests();
        reqRecyclerView = view.findViewById(R.id.friendsReqRecyclerView);
        reqRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        reqRecyclerView.setItemAnimator(new DefaultItemAnimator());
        friendReqRecyclerAdapter reqAdapter=new friendReqRecyclerAdapter(view.getContext(), user,this);
        reqRecyclerView.setAdapter(reqAdapter);
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onClick(int position, boolean accept) {
        Log.d(TAG, "onClick: user " + user.getFriendRequests().get(position).getUsername() + (accept?"accepted":"rejected"));
        Thread sendReply = new Thread(() -> {
            Socket socket;
            StringBuilder reply = new StringBuilder();
            reply.append("friendRequestReply,").append(user.getFriendRequests().get(position).getUsername())
                    .append(",").append(accept ? "true," : "false,").append(user.getUsername());
            try {
                socket = new Socket(getActivity().getResources().getString(R.string.ip), 8080);
                DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
                dos.writeUTF(reply.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        sendReply.start();
        try {
            sendReply.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "onClick: before dataset change friends req:"+user.getFriendRequests().size());
        Log.d(TAG, "onClick: before dataset change friends:"+user.getFriends().size());
        refreshUser refreshuser=new refreshUser(getActivity());
        user=refreshuser.refresh(user.getUsername());
        ((friendReqRecyclerAdapter)reqRecyclerView.getAdapter()).updateList(user.getFriendRequests());

        Log.d(TAG, "onClick: after dataset change friends req:"+user.getFriendRequests().size());
        Log.d(TAG, "onClick: after dataset change friends:"+user.getFriends().size());
       /* reqRecyclerView.getAdapter().notifyItemRemoved(position);
        reqRecyclerView.getAdapter().notifyDataSetChanged();*/
        //reqRecyclerView.getAdapter().notifyDataSetChanged();
        //reqRecyclerView.getAdapter().notifyItemRangeChanged(position,user.getFriendRequests().size());

        Log.d(TAG, "onClick: user friend request size"+user.getFriendRequests().size());
        Log.d(TAG, "onClick: user friend size"+user.getFriends().size());
    }
}
interface RequestListener{
    void onRequestEvent();
}
